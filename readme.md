#Jekyll Base

Nice starting point with Jekyll

##Features:

* ###[Grunt](http://gruntjs.com/)
* ###[Bower](http://bower.io/)
* ###[Jekyll](http://jekyllrb.com/)

##Folder Structure:

	├─ assets
	│  ├─ img
	│  ├─ js
	│  ├─ img
	│
	├─ jekyll
	│  ├─ _includes
	│  ├─ _layouts
	│  ├─ _posts
	│  ├─ _site
	│
	├─ run_control
		
##Installation:

###Tools for the job:

* [Git](http://git-scm.com/download/)
* [Node.js](http://nodejs.org/)
* [Grunt](http://gruntjs.com/getting-started)
* [Bower](http://bower.io/)
* [Jekyll](http://jekyllrb.com/)
* [Terminal notifier](https://github.com/alloy/terminal-notifier) - For grunt:growl

###Setup:

* **Navigate** to your working directory 
	* `cd /path/to/your/project`
* **Install** [Node Package Manager](https://www.npmjs.org/doc/install.html)
	* `sudo npm install`
* **Install** Bower components
	* `bower install`
	
###Grunt tasks:

* **Grunt Develop**
	* `grunt develop`	
* **Grunt Build**
	* `grunt build`	
	
##Things to do

* ~~Strikethrough something~~