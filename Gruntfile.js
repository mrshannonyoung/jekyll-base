'use strict';
module.exports = function(grunt) {

	// Load all grunt tasks matching the `grunt-*` pattern
	require('load-grunt-tasks')(grunt);

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		// Directories
			dirs: {
				jekyll:		'jekyll',
				site:		'jekyll/_site',
				assets:		'assets',
				bower:		'assets/bower',
				rc:			'run_control'
			},
		// Jekyll
			jekyll: {
				options: {
					bundleExec:	false,
					src: '<%= dirs.jekyll %>'
				},
				dev: {
					options: {
						config: '_config_dev.yml',
						dest:	'<%= dirs.site %>'
					}
				},
				build: {
					options: {
						config: '_config.yml',
						dest:	'<%= dirs.site %>'
					}
				}
			},	
		// CSS
			sass: {
			    dev: {
			    	options: {                       
						style: 'expanded',
						sourcemap: 'auto',
						lineNumbers: true
					},
					files: {
						'<%= dirs.site %>/css/style.css': '<%= dirs.assets %>/scss/base.scss'
					}
			    },
			    dist: {
			    	options: {                       
						style: 'compressed',
						sourcemap: 'none'
					},
					files: {
						'<%= dirs.site %>/css/style.css': '<%= dirs.assets %>/scss/base.scss'
					}
			    }
			},
			autoprefixer: {
				files: {
					src:	'<%= dirs.site %>/css/style.css',
					dest:	'<%= dirs.site %>/css/style.css'
				}
			},
			csslint: {
				options: {
					csslintrc: '<%= dirs.rc %>/.csslintrc',
					import: 2
				},				
				files: {
					src: '<%= dirs.site %>/**/*.css'
				}
			},
		// Javascript
			concat: {
				files: {
					src:	'<%= dirs.assets %>/js/custom/*.js',
					dest:	'<%= dirs.assets %>/js/scripts.js'
				}
			},
			uglify: {
				dev: {
					files: {
						'<%= dirs.site %>/js/min/scripts.js':	['<%= dirs.assets %>/js/scripts.js'],
						'<%= dirs.site %>/js/min/vendor.js':	[
																	'<%= dirs.assets %>/bower/jquery/dist/jquery.js',
																	'<%= dirs.assets %>/bower/modernizr/modernizr.js'
																]
					}
				}
			},
			jshint: {
				options: {
					// External file
					jshintrc: '<%= dirs.rc %>/.jshintrc',
					force: true
				},
				all: ['<%= dirs.site %>/js/*.js']
			},
		// Images
			imagemin: {
				svg: {
					files: [
						{
							expand: true,
							cwd:	'<%= dirs.assets %>/img/',
							src:	['*.svg'],
							dest:	'<%= dirs.site %>/img/optimised/',
							ext:	'.svg'
						}
					]
				},				
				png: {
					options: {
						optimizationLevel: 7
					},
					files: [
						{
							expand: true,
							cwd:	'<%= dirs.assets %>/img/',
							src:	['*.png'],
							dest:	'<%= dirs.site %>/img/optimised/',
							ext:	'.png'
						}
					]
				},
				jpg: {
					options: {
						progressive: true
					},
					files: [
						{
							expand:	true,
							cwd:	'<%= dirs.assets %>/img/',
							src:	['**/*.{jpeg,jpg}'],
							dest:	'<%= dirs.site %>/img/optimised/',
							ext:	'.jpg'
						}
					]
				}
			},
		// HTML Minification
			htmlmin: {
				dist: {
					options: {
						removeComments: true,
						collapseWhitespace: true
					},					
					files: [
						{
							expand: true,
							cwd: '<%= dirs.site %>',
							src: ['**/*.html'],
							dest: '<%= dirs.site %>',
							ext: '.html',
						}
					]
				}
			},			
		// HTML
			htmlhint: {
				options: {
					htmlhintrc: '<%= dirs.rc %>/.htmlhintrc',
					force: true
				},
				files: {
					src: '<%= dirs.site %>/**/*.html'
				}
			},
		// Copy
			copy: {
				fonts: {
					files:
					[{
						expand: true,
						cwd: '<%= dirs.assets %>/fonts/',
						src: ['**'],
						dest: '<%= dirs.site %>/fonts/',
					}],
				},
				video: {
					files:
					[{
						expand: true,
						cwd: '<%= dirs.assets %>/video/',
						src: ['**'],
						dest: '<%= dirs.site %>/video/',
					}],
				},
			},		
		// Tasks
			watch: {
				sass: {
					files: ['<%= dirs.assets %>/scss/**/*.scss'],
					tasks: ['sass:dev', 'autoprefixer', 'csslint', 'growl:dev']
				},
				js: {
					files: ['Gruntfile.js','<%= dirs.assets %>/js/**/*.js'],
					tasks: ['concat', 'uglify', 'jshint', 'growl:dev']
				},
				images: {
					files: ['<%= dirs.assets %>/img/**/*.*'],
					tasks: ['imagemin', 'growl:dev']
				},
				html: {
					files: ['*.yml','<%= dirs.jekyll %>/**/*.html', '<%= dirs.jekyll %>/**/*.md', '<%= dirs.jekyll %>/**/*.yml'],
					tasks: ['jekyll:dev', 'htmlhint', 'sass:dev', 'autoprefixer', 'concat', 'uglify', 'imagemin', 'copy', 'growl:dev']
				}
			},		
			growl: {
				dev: {
					title:		"Success \u2714",
					message:	"Development mode - ran using 'develop' task"
				},
				dist: {
					title:		"Success \u2714",
					message:	"Build complete - ran using 'build' task"
				}
			}
	});

	grunt.registerTask('develop',	[
										'growl:dev',
										'watch'
									]
	);

	grunt.registerTask('build',		[
										'jekyll:build',
										'htmlmin:dist',
										'sass:dist',
										'autoprefixer',
										'concat',
										'uglify',
										'imagemin',
										'copy',
										'growl:dist'
									]
	);
	
	// Set the grunt force option
	grunt.option('force', true);

};